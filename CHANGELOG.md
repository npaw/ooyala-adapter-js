## [6.5.5] - 2020-02-14
### Added
- youboraPluginInstance object in window referencing the plugin instance, when creating it in ooyala plugins framework
### Library
- Packaged with `lib 6.5.26`

## [6.5.4] - 2019-12-19
### Added
- 'noMonitor' option to disable buffer monitor
### Library
- Packaged with `lib 6.5.22`

## [6.5.3] - 2019-12-13
### Library
- Packaged with `lib 6.5.20`

## [6.5.2] - 2019-08-16
### Library
- Packaged with `lib 6.5.11`

## [6.5.1] - 2019-07-30
### Added
- Listener for source change
### Library
- Packaged with `lib 6.5.9`

## [6.5.0] - 2019-07-26
### Added
- Support for new ad metrics
### Library
- Packaged with `lib 6.5.7`

## [6.4.2] - 2019-01-03
### Library
- Packaged with `lib 6.4.12`

## [6.4.0] - 2018-09-17
### Added
- Error detected as fatal when code is network_error
### Library
- Packaged with `lib 6.4.7`

## [6.3.0] - 2018-06-21
### Added
- Creating new view when listening INITIAL_PLAYBACK_REQUESTED

## [6.2.3] - 2018-05-28
### Added
- Creating views when going to foreground

## [6.2.2] - 2018-05-28
### Added
- Support for old version player events with iphone

## [6.2.1] - 2018-05-23
### Library
- Packaged with `lib 6.2.6`

## [6.2.0] - 2018-04-09
### Library
- Packaged with `lib 6.2.0`

## [6.1.0] - 2018-02-27
### Library
- Packaged with `lib 6.1.12`
