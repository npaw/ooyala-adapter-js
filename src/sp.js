var youbora = require('youboralib')
require('./adapter')
require('./analyticsFramework')(youbora)

module.exports = youbora
