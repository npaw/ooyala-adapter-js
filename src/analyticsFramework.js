/* global OO */
var manifest = require('../manifest.json')

module.exports = function (youbora) {
  /**
       * @class YBOOAnalyticsPlugin
       * @classdesc This is an example class of a plugin that works with the Ooyala Analytics Framework.
       * @param {object} framework The Analytics Framework instance
       */
  youbora.AnalyticsFramework = function (framework) {
    this.framework = framework
    this.name = 'youbora'
    this.version = manifest.version
    this.id = null
    this.active = true

    /**
     * [Required Function] Return the name of the plugin.
     * @public
     * @method YBOOAnalyticsPlugin#getName
     * @return {string} The name of the plugin.
     */
    this.getName = function () {
      return this.name
    }

    /**
     * [Required Function] Return the version string of the plugin.
     * @public
     * @method YBOOAnalyticsPlugin#getVersion
     * @return {string} The version of the plugin.
     */
    this.getVersion = function () {
      return this.version
    }

    /**
     * [Required Function] Set the plugin id given by the Analytics Framework when
     * this plugin is registered.
     * @public
     * @method YBOOAnalyticsPlugin#setPluginID
     * @param  {string} newID The plugin id
     */
    this.setPluginID = function (newID) {
      this.id = newID
    }

    /**
     * [Required Function] Returns the stored plugin id, given by the Analytics Framework.
     * @public
     * @method YBOOAnalyticsPlugin#setPluginID
     * @return  {string} The pluginID assigned to this instance from the Analytics Framework.
     */
    this.getPluginID = function () {
      return this.id
    }

    /**
     * [Required Function] Initialize the plugin with the given metadata.
     * @public
     * @method YBOOAnalyticsPlugin#init
     */
    this.init = function () {
      var missedEvents
      // if you need to process missed events, here is an example
      if (this.framework) {
        missedEvents = this.framework.getRecordedEvents()
        for (var k in missedEvents) {
          var event = missedEvents[k]
          this.plugin.getAdapter().processEvent(event.eventName, event.params)
        }
      }
    }

    /**
     * [Required Function] Set the metadata for this plugin.
     * @public
     * @method YBOOAnalyticsPlugin#setMetadata
     * @param  {object} metadata The metadata for this plugin
     */
    this.setMetadata = function (metadata) {
      youbora.Log.notice('Ooyala Analytics Framework Plugin ' + this.id + ' is ready.')
      if (!this.plugin) {
        this.plugin = new youbora.Plugin(metadata)
        window.youboraPluginInstance = this.plugin
        this.plugin.setAdapter(new youbora.adapters.Ooyala())
      } else {
        this.plugin.setOptions(metadata)
      }
      if (typeof metadata.noMonitor !== 'undefined' && metadata.noMonitor) {
        this.plugin.getAdapter().monitor.stop()
        this.plugin.getAdapter().monitor = null
      }
      if (typeof metadata.debug !== 'undefined') {
        youbora.Log.logLevel = metadata.debug
      }
    }

    /**
     * [Required Function] Process an event from the Analytics Framework, with the given parameters.
     * @public
     * @method YBOOAnalyticsPlugin#processEvent
     * @param  {string} eventName Name of the event
     * @param  {Array} params     Array of parameters sent with the event
     */
    this.processEvent = function (eventName, params) {
      this.plugin.getAdapter().processEvent(eventName, params)
    }

    /**
     * [Required Function] Clean up this plugin so the garbage collector can clear it out.
     * @public
     * @method YBOOAnalyticsPlugin#destroy
     */
    this.destroy = function () {
      delete this.framework
      delete this.plugin
    }
  }

  if (typeof OO !== 'undefined') {
    if (OO.Analytics) {
      OO.Analytics.RegisterPluginFactory(youbora.AnalyticsFramework)
    } else {
      youbora.Log.error('OO.Analytics not found. This plugin is designed for Ooyala V4.')
    }
  } else {
    youbora.Log.error('Ooyala library not loaded yet.')
  }
}
