var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Ooyala = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    if (this.player && this.player.getPlayheadTime) {
      return this.player.getPlayheadTime() || this.playhead
    }
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    if (this.player && this.player.getDuration) {
      return this.player.getDuration() || this.duration
    }
    return this.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    if (this.player && this.player.getCurrentBitrate) {
      return this.player.getCurrentBitrate() || this.bitrate
    }
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    return this.rendition
  },

  /** Override to return title */
  getTitle: function () {
    if (this.player && this.player.getTitle) {
      return this.player.getTitle() || this.title
    }
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.isLive
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.resource || 'unknown'
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return OO.VERSION.core.rev
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Ooyala'
  },

  registerListeners: function () {
    this.monitorPlayhead(true, false)
  },

  unregisterListeners: function () {
    this.monitor.stop()
  },

  processEvent: function (eventName, params) {
    try {
      var Event = OO.Analytics.EVENTS
      var Events = OO.EVENTS
      if (eventName !== Event.VIDEO_STREAM_POSITION_CHANGED && eventName !== Event.VIDEO_STREAM_DOWNLOADING) youbora.Log.debug(eventName)
      if (!this.isShowingAds) {
        switch (eventName) {
          case Event.AD_REQUEST:
            if (!this.plugin.getAdsAdapter()) {
              this.plugin.setAdsAdapter(new youbora.adapters.Ooyala.AdsAdapter(this.player))
              this.adsAdapter = this.plugin.getAdsAdapter()
            }
            break
          case Event.VIDEO_SOURCE_CHANGED:
            this.fireStop()
            this.fireStart()
            break
          case Event.VIDEO_STREAM_POSITION_CHANGED:
            this.playhead = params[0].streamPosition
            this.duration = params[0].totalStreamDuration
          case Event.VIDEO_STREAM_DOWNLOADING:
            if (!this.crashed && this.playhead) {
              if (this.playhead < this.getDuration() - 1) {
                this.fireStart()
              }
            }
            if (this.getPlayhead() > 0.1) {
              this.fireJoin()
            }
            if (this.seekReady) {
              this.fireSeekEnd()
            }
            break
          case Event.INITIAL_PLAYBACK_REQUESTED:
            if (this.flags.isJoined) {
              this.fireStop()
            }
          case Event.VIDEO_PLAY_REQUESTED:
          case Event.VIDEO_REPLAY_REQUESTED:
            if (!this.flags.isStarted) {
              this.playhead = null
            }
            if (!this.plugin.getAdsAdapter()) {
              this.plugin.setAdsAdapter(new youbora.adapters.Ooyala.AdsAdapter(this.player))
              this.adsAdapter = this.plugin.getAdsAdapter()
            }
            if (!this.crashed && this.playhead < this.getDuration() - 1) {
              this.fireStart()
            }
            this.crashed = false
            break
          case Event.VIDEO_BUFFERING_ENDED:
          case Event.VIDEO_STREAM_DOWNLOADING:
            if (this.getPlayhead() > 0.1) {
              this.fireJoin()
            }
            break
          case Event.VIDEO_PAUSE_REQUESTED:
          case Event.VIDEO_PAUSED:
          case Events.PAUSED:
            this.firePause()
            break
          case Event.INITIAL_PLAY_STARTING:
          case Event.VIDEO_PLAYING:
          case Events.PLAYING:
            if (this.getPlayhead() > 0.1) {
              this.fireJoin()
            }
            this.fireResume()
          case Event.VIDEO_SEEK_COMPLETED:
          case Event.VIDEO_BUFFERING_ENDED:
            this.seekReady = true
            break
          case Events.SEEK:
          case Event.VIDEO_BUFFERING_STARTED:
            if (!this.plugin.deviceDetector.isIphone()) {
              break
            }
          case Event.VIDEO_SEEK_REQUESTED:
            this.fireSeekBegin()
            this.seekReady = false
            break
          case Event.AD_BREAK_STARTED:
            this.firePause()
            this.isShowingAds = true
            break
          case Event.DESTROY:
          case Event.PLAYBACK_COMPLETED:
            this.fireStop()
            break
          case Event.VIDEO_CONTENT_METADATA_UPDATED:
            this.title = params[0].title
            this.duration = params[0].duration / 1000
            break
          case Event.VIDEO_ELEMENT_CREATED:
            this.resource = params[0].streamUrl
            break
          case Event.STREAM_TYPE_UPDATED:
            this.isLive = !(params[0].streamType == 'vod')
            break
          case Event.BITRATE_INITIAL:
          case Event.VIDEO_STREAM_BITRATE_CHANGED:
            this.bitrate = params[0].bitrate
            if (isNaN(params[0].id)) {
              this.rendition = params[0].id
            } else if (params[0].width && params[0].height && params[0].bitrate) {
              this.rendition = youbora.Util.buildRenditionString(params[0].width, params[0].height, params[0].bitrate)
            } else {
              this.rendition = null
            }
            break
          case Events.ERROR:
          case Event.ERROR.AUTHORIZATION:
          case Event.ERROR.VIDEO_PLAYBACK:
            this.fireError(params[0].errorCode, params[0].errorMessage)
            if (params[0].errorCode === 'stream' || params[0].errorCode === 'network_error') {
              this.fireStop()
              this.crashed = true
            }
            break
          case Event.AD_BREAK_STARTED:
          case Event.AD_REQUEST_EMPTY:
          case Event.AD_REQUEST_ERROR:
          case Event.AD_REQUEST_SUCCESS:
            this.adsAdapter.processEvent(eventName, params)
            break
        }
      }
      if (this.adsAdapter && this.isShowingAds) this.adsAdapter.processEvent(eventName, params)
    } catch (error) {
      youbora.Log.error(error)
    }
  }
},
{
  AdsAdapter: require('./ads/adsadapter')
}
)

module.exports = youbora.adapters.Ooyala
