var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech + '-ads'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = this.playhead
    if (this.player && this.player.getPlayheadTime) {
      ret = this.player.getPlayheadTime() || this.playhead
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = this.duration
    if (this.player && this.player.getDuration) {
      ret = this.player.getDuration() || this.duration
    }
    return ret
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = this.bitrate
    if (this.player && this.player.getCurrentBitrate) {
      ret = this.player.getCurrentBitrate() || this.bitrate
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    return this.rendition
  },

  /** Override to return title */
  getTitle: function () {
    var ret = this.title
    if (this.player && this.player.getTitle) {
      ret = this.player.getTitle() || this.title
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    return 'unknown'
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return OO.VERSION.core.rev
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Ooyala'
  },

  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Midroll
    if (!this.plugin._adapter.getDuration()) {
      if (!this.plugin._adapter.flags.isJoined) {
        ret = youbora.Constants.AdPosition.Preroll
      }
    } else {
      if (this.plugin._adapter.getPlayhead() === this.plugin._adapter.getDuration()) {
        ret = youbora.Constants.AdPosition.Postroll
      } else if (!this.plugin._adapter.getPlayhead()) {
        ret = youbora.Constants.AdPosition.Preroll
      }
    }
    return ret
  },

  getGivenAds: function () {
    return this.breakAds
  },

  processEvent: function (eventName, params) {
    try {
      var Event = OO.Analytics.EVENTS
      switch (eventName) {
        case Event.AD_REQUEST_EMPTY:
          this.fireManifest(youbora.Constants.ManifestError.EMPTY, 'Empty manifest')
          break
        case Event.AD_REQUEST_ERROR:
          this.fireManifest(youbora.Constants.ManifestError.WRONG, 'Wrong manifest format')
          break
        case Event.AD_REQUEST_SUCCESS:
          this.fireManifest()
          break
        case Event.AD_POD_STARTED:
          this.breakAds = params[0].numberOfAds
          this.fireBreakStart()
          break
        case Event.VIDEO_STREAM_POSITION_CHANGED:
          this.fireJoin()
          this.playhead = params[0].streamPosition
          this.duration = params[0].totalStreamDuration
          if (this.flags.isPaused && (this.playhead > this.clickPlayhead)) {
            this.clickPlayhead = null
            this.fireResume()
          }
          // Quartile
          if (this.duration) {
            if (this.playhead > this.duration / 4) {
              if (!this.firstQuartile) {
                this.firstQuartile = true
                this.fireQuartile(1)
              }
              if (this.playhead > this.duration / 2) {
                if (!this.secondQuartile) {
                  this.secondQuartile = true
                  this.fireQuartile(2)
                }
                if (this.playhead > this.duration * 0.75) {
                  if (!this.thirdQuartile) {
                    this.thirdQuartile = true
                    this.fireQuartile(3)
                  }
                }
              }
            }
          }
          break
        case Event.AD_STARTED:
          this.playhead = 0
          if (params[0].adMetadata) {
            this.duration = params[0].adMetadata.adDuration
            this.title = params[0].adMetadata.adId
          }
          if (!this.plugin.getAdapter().flags.isStarted) this.plugin.getAdapter().fireStart()
          this.fireStart()
          this.monitorPlayhead(true, false, 1200)
          break
        case Event.AD_POD_ENDED:
        case Event.AD_BREAK_ENDED:
          this.fireBreakStop()
          this.plugin._adapter.isShowingAds = false
          this.plugin._adapter.fireResume()
        case Event.AD_ENDED:
        case Event.AD_COMPLETED:
          this.fireStop()
          this.resetValues()
          break
        case Event.AD_SKIPPED:
          this.fireSkip()
          break
        case Event.VIDEO_PLAY_REQUESTED:
          this.fireResume()
          break
        case Event.VIDEO_PLAYING:
        case Event.AD_IMPRESSION:
          this.fireResume()
          this.fireJoin()
          break
        case Event.VIDEO_PAUSED:
          // this.firePause()
          break
        case Event.VIDEO_CONTENT_METADATA_UPDATED:
          this.title = params[0].title
          this.duration = params[0].duration / 1000
          break
        case Event.BITRATE_INITIAL:
          this.bitrate = params[0].bitrate
          if (isNaN(params[0].id)) {
            this.rendition = params[0].id
          } else if (params[0].width && params[0].height && params[0].bitrate) {
            this.rendition = youbora.Util.buildRenditionString(params[0].width, params[0].height, params[0].bitrate)
          } else {
            this.rendition = null
          }
          break
        case Event.AD_ERROR:
          this.fireError('ad_error', params[0].error)
          this.fireStop()
          this.resetValues()
          this.playhead = 0
          this.plugin._adapter.isShowingAds = false
          break
        case Event.AD_CLICKTHROUGH_OPENED:
          this.clickPlayhead = this.playhead
          this.firePause()
          this.fireClick()
          break
      }
    } catch (error) {
      youbora.Log.error(error)
    }
  },

  resetValues: function () {
    this.firstQuartile = false
    this.secondQuartile = false
    this.thirdQuartile = false
  }
})

module.exports = AdsAdapter
